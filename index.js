(function () {

  Object.prototype[Symbol.toPrimitive] = function (hint) {
    let res;

    if (hint == 'string') {
      res = this.toString();
      if (typeof res === 'string') {
        console.log('type of transformation: string');
        return res;
      } else if (typeof res !== 'string') {
        res = this.valueOf();
        if (typeof res === 'number') {
          console.log('type of transformation: number');
          return res;
        } else if (typeof res !== 'string' && typeof res !== 'number') {
          throw new TypeError();
        }
      }
    }

    if (hint == 'number') {
      res = this.valueOf();
      if (typeof res === 'number') {
        console.log('type of transformation: number');
        return res;
      }
      else if (typeof res !== 'number') {
        res = this.toString();
        if (typeof res === 'string') {
          console.log('type of transformation: string');
          return res;
        } else if (typeof res !== 'string' && typeof res !== 'number') {
          throw new TypeError();
        }
      }
    }

    if (hint == 'default') {
      console.log('type of transformation: default');
      return this.toString();
    }
  };
  user = { user: 'miha' };
  admin = { admin: true };
  a = function () { };
  console.log(String(admin));
  console.log(+[]);
  console.log(Number(admin));

})();